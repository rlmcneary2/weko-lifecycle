import { describe, expect, it, vi } from "vitest";
import "@testing-library/jest-dom/vitest";
import { screen } from "@testing-library/dom";
import { create } from "./elementBuilder/elementBuilder";
import { WekoLifecycleMixin } from "./WekoLifecycle.ts";
import { WEKO_CHANGED_EVENT } from "./shared.ts";

let uid = Date.now();

/**
 * Similar to testing-library/react prefer to test the DOM results, rather than the invocation of
 * functions.
 */

describe("WekoMixin connected", () => {
  it("emits connected when appended", () => {
    const uid = getUid();
    const elem = document.createElement("test-component");
    elem.textContent = uid;

    const mock = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, mock);

    document.body.append(elem);
    expect(screen.getByText(uid)).toBeInTheDocument();
    expect(mock).toHaveBeenCalledOnce();
    expect(mock).toHaveBeenLastCalledWith(
      expect.objectContaining({ detail: { type: "connected" } })
    );
  });

  it("emits disconnected when removed", () => {
    const uid = getUid();
    const elem = document.createElement("test-component");
    elem.textContent = uid;

    const mock = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, mock);

    document.body.append(elem);
    expect(screen.getByText(uid)).toBeInTheDocument();
    expect(mock).toHaveBeenCalledOnce();
    expect(mock).toHaveBeenLastCalledWith(
      expect.objectContaining({ detail: { type: "connected" } })
    );

    elem.remove();
    expect(screen.queryByText(uid)).not.toBeInTheDocument();
    expect(mock).toHaveBeenCalledTimes(2);
    expect(mock).toHaveBeenLastCalledWith(
      expect.objectContaining({ detail: { type: "disconnected" } })
    );
  });

  it("emits connected and disconnected when moved", () => {
    const parent1 = create("div", { properties: { id: "parent1" } });
    document.body.append(parent1);
    const parent2 = create("div", { properties: { id: "parent2" } });
    document.body.append(parent2);

    const uid = getUid();
    const elem = document.createElement("test-component");
    elem.textContent = uid;

    const mock = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, mock);

    parent1.append(elem);
    expect(screen.getByText(uid)).toBeInTheDocument();
    expect(mock).toHaveBeenCalledOnce();
    expect(mock).toHaveBeenLastCalledWith(
      expect.objectContaining({ detail: { type: "connected" } })
    );

    parent2.append(elem);
    expect(screen.getByText(uid)).toBeInTheDocument();
    expect(mock).toHaveBeenCalledTimes(3);
    expect(mock).toHaveBeenNthCalledWith(
      2,
      expect.objectContaining({ detail: { type: "disconnected" } })
    );
    expect(mock).toHaveBeenLastCalledWith(
      expect.objectContaining({ detail: { type: "connected" } })
    );
  });
});

describe("WekoMixin attributes", () => {
  it("batches attribute changes", async () => {
    const uid = getUid();
    const elem = document.createElement("test-component");
    elem.textContent = uid;

    const listener = vi.fn();
    elem.addEventListener(WEKO_CHANGED_EVENT, listener);

    document.body.append(elem);
    expect(listener).toHaveBeenCalledOnce();
    expect(listener).toHaveBeenLastCalledWith(
      expect.objectContaining({ detail: { type: "connected" } })
    );

    elem.setAttribute("apples", "1");
    elem.setAttribute("oranges", "2");

    await new Promise<void>(resolve => setTimeout(() => resolve(), 0));

    // console.log(listener.mock.calls.map(i => JSON.stringify(i[0].detail, null, 2)));

    expect(listener).toHaveBeenCalledTimes(2);
    expect(listener).toHaveBeenLastCalledWith(
      expect.objectContaining({
        detail: {
          attributes: {
            apples: { newValue: "1", oldValue: null },
            oranges: { newValue: "2", oldValue: null }
          },
          type: "attributes"
        }
      })
    );
  });
});

class TestComponent extends WekoLifecycleMixin(HTMLElement) {
  constructor() {
    super();
  }

  static get observedAttributes(): readonly string[] {
    // @ts-ignore
    return [...(super.observedAttributes ?? []), "apples", "oranges"];
  }
}

customElements.define("test-component", TestComponent);

function getUid(): string {
  uid += 1;
  return "_" + uid;
}
