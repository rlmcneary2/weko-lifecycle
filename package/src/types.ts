/**
 * This detail object is emitted in a {@link WEKO_CHANGED_EVENT} {@link !CustomEvent} when:
 *
 * - the component is connected
 * - the component is disconnected
 * - attributes changed; see {@link WekoChangedEventAttributeDetails}.
 *
 * @remarks
 * This detail is included in the `weko-changed-event` ({@link WEKO_CHANGED_EVENT}). Add an event
 *   listener to an element produced by {@link WekoLifecycleMixin} to be informed when attributes
 *   change.
 *
 * @example Subscribe to changes
 * ```ts
 * this.addEventListener(WEKO_CHANGED_EVENT, evt => {
 *   console.log(evt.detail);
 *   // "{ ...WekoChangedEventDetails }"
 * });
 * ```
 */
export interface WekoChangedEventDetails {
  /**
   * The type of the WekoChangedEvent.
   *
   * - "attributes" - emitted when `attributeChangedCallback` is invoked
   * - "connected" - dispatched when `connectedCallback` is called
   * - "disconnected" - emitted when `disconnectedCallback` is invoked
   */
  type: "attributes" | "connected" | "disconnected";
}

/**
 * This detail object extends {@link WekoChangedEventDetails} and is emitted in a
 * {@link WEKO_CHANGED_EVENT} {@link !CustomEvent} when one or more of a component's attributes have
 * changed.
 *
 * @remarks
 * This detail is included in the `weko-changed-event` ({@link WEKO_CHANGED_EVENT}). Emitted when a
 *  component's attributes change.
 *
 * The value of this event's `type` property will be "attributes".
 *
 * - Add an event listener to an element produced by {@link WekoLifecycleMixin} to be informed when
 *   attributes change.
 *
 * @example subscribe to changes
 * ```ts
 * this.addEventListener(WEKO_CHANGED_EVENT, evt => {
 *   console.log(evt.detail);
 *   // "{ ...WekoChangedEventAttributeDetails }"
 * });
 * ```
 */
export interface WekoChangedEventAttributeDetails extends WekoChangedEventDetails {
  /** The type of the WekoChangedEventAttributeDetails. */
  type: "attributes";
  /**
   * A collection of attributes that have changed and their values.
   *
   * @remarks
   * This event will be the result of batching all the attribute changes that occur in a single
   * task. The key of the `Record` is the name of an attribute that changed. The values in a
   * `Record` contain both the old and new values of the attribute.
   *
   * To be notified when an attribute changes, the attribute name must be included in the static
   * [`observedAttributes`](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements#responding_to_attribute_changes)
   * web component lifecycle property of the web component.
   *
   * @example event detail for attributes
   * ```ts
   * {
   *   attributes: {
   *     height: {
   *       newValue: "100px", oldValue: "110px"
   *     },
   *     class: {
   *       newValue: "button primary", oldValue: null
   *     }
   *   },
   *   type: "attributes"
   * }
   * ```
   */
  attributes: Record<
    string,
    {
      /** The attribute's new value. */
      newValue: unknown;
      /** The attribute's previous value. */
      oldValue: unknown;
    }
  >;
}

/**
 * The interface implemented by a WekoLifecycleType instance (probably the WekoLifecycle class).
 */
export interface WekoLifecycleType {}
