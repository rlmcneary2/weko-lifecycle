# @rlmcneary2/weko-lifecycle

Here at **weko** we're bringing the fun back to creating for the web!

All our tools:

- Run directly in the web browser! No preprocessor or bundling needed.
- Use standard web technologies like module imports and web components.

We believe in the power of the web⚡and are making it as easy to create powerful web pages today as
it was back in 1993. Open a text editor and start building!

Other weko packages that make web components better:

- **[weko-render](https://codeberg.org/rlmcneary2/weko-render)** — simplify custom element DOM manipulation and
  performance.
- **[weko-routing](https://codeberg.org/rlmcneary2/weko-routing)** — manipulate and respond to
  changes to the browser's URL.

See the example app (TODO) to learn how to utilize web browsers' modern features while building
awesome web applications that don't require extensive tech stacks just for bundling.

**Contents**

- [Usage](#usage)
- [API](#api)

## Usage

This package informs the client of [web component life
cycle](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements#custom_element_lifecycle_callbacks)
changes through an event. This simplifies the lifecycle API and reduces boilerplate code.

👉 _The weko-lifecycle package includes TypeScript type support._

### Add lifecycle functionality

#### Custom element

To get started, extend a custom element class by invoking `WekoLifecycleMixin` and passing it the
base type, which must be `HTMLElement` or a class that extends `HTMLElement`. You can even pass
another weko mixin as the `baseType`.

👉 _All the examples are written in TypeScript, but JavaScript is also supported._

```ts
import { WekoLifecycleMixin } from "https://esm.sh/@rlmcneary2/weko-lifecycle";

class PeopleComponent extends WekoLifecycleMixin(HTMLElement) {
  constructor() {
    super();
  }

  // class implementation
}

// Register your web component.
if (!window.customElements.get("wc-people")) {
  window.customElements.define("wc-people", PeopleComponent);
}
```

### Listening for lifecycle changes

Connect your web component to the `weko-changed-event` event. The event is dispatched on the
component itself, so add the listener to the component using `this`.

```ts
import {
  WEKO_CHANGED_EVENT,
  WekoLifecycleMixin,
  isWekoChangedEvent
} from "https://esm.sh/@rlmcneary2/weko-routing";

class PeopleComponent extends WekoLifecycleMixin(HTMLElement) {
  constructor() {
    super();

    this.addEventListener(WEKO_CHANGED_EVENT, this.#handleChangedEvent);
  }

  #handleChangedEvent(evt: Event): void {
    if (isWekoChangedEvent(evt)) {
      if (evt.detail.type === "connected") {
        console.log("Connected!");
      }
    }
  }

  // class implementation
}
```

Now you're ready to respond to web component lifecycle changes!

## API

There is detailed information about the mixin components and interfaces in the [API
Documentation](https://rlmcneary2.codeberg.page/weko-lifecycle/docs/)

## Installation

### As part of an ES module

Use "esm.sh" and import this module into a source file where you need to use
routing.

```ts
import from "https://esm.sh/@rlmcneary2/weko-lifecycle";
```

### As part of a bundle

If you are bundling your source code you may need to use a dynamic `import` to
load the library like so:

```ts
async function main() {
  await import("https://esm.sh/@rlmcneary2/weko-lifecycle");
}
```

### Using a script element

The script can also be loaded in an HTML file, typically as part of the `<head>`
element.

```html
<head>
  <script src="https://esm.sh/@rlmcneary2/weko-lifecycle" type="module"></script>
</head>
```

### TypeScript support

To use Typescript during development you need to do three things.

1. Install the package locally from npm under devDependencies: <span style="white-space:nowrap">`npm
i -D @rlmcneary2/weko-lifecycle`</span>.
2. Update the tsconfig.json file's `compilerOptions.paths` with the local
   installation of the package.

```json
"paths": {
  "@rlmcneary2/weko-lifecycle": ["./node_modules/@rlmcneary2/weko-lifecycle/src/index"]
}
```

3. Create a `.d.ts` file somewhere in a path processed by tsconfig.json that
   declares a module for the remote address.

```ts
// remote.d.ts

// Allow all module names starting with "https://". This will suppress TS errors.
declare module "https://*";

// Map the URL to all the exports from the locally installed package.
declare module "https://esm.sh/@rlmcneary2/weko-lifecycle" {
  export * from "@rlmcneary2/weko-lifecycle";
}
```
